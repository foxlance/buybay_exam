# BuyBay - Product Destination Finder

An application that allows routing of products to their proper destination by defining criteria definitions.

### Requirement
 - Node v16.1.0
 - Yarn v1.22.10
 - Ruby v2.7.2
 - Rails v6.1.3.2
 - PostgreSQL

### Configuration
Please edit `.env.development` and `.env.test` accordingly

### Database creation
```
$ createdb buybay_exam_development
$ createdb buybay_exam_test
```

### Database initialization
```
$ bin/rails db:migrate
```

### How to run the test suite
```
$ rspec
```
