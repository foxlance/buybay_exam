class CriteriaDefinitionDecorator < BaseDecorator
  def print_references
    join_values(references)
  end

  def print_categories
    join_values(categories)
  end

  def references_badge
    badgefy(references)
  end

  def categories_badge
    badgefy(categories)
  end

  private

  def join_values(values)
    values.join(', ') unless values.blank?
  end

  def badgefy(values)
    return unless values.present?

    values.map do |value|
      "<span class='badge bg-secondary'>#{value}</span>"
    end.join(' ').html_safe
  end
end
