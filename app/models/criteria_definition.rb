class CriteriaDefinition < ApplicationRecord
  validates_presence_of :destination

  def references=(value)
    super(split_values(value))
  end

  def categories=(value)
    super(split_values(value))
  end

  def self.matches_any_reference(reference)
    where("'#{reference}' = ANY (\"references\")")
  end

  def self.contains_any_category(category)
    where('categories && ?', "{#{category}}")
  end

  def self.less_than_price(price)
    no_price_cap.or(self.where("maximum_price >= ?", price))
  end

  def self.no_price_cap
    where("maximum_price IS NULL")
  end

  private

  def split_values(value)
    value.split(',').map(&:strip) unless value.blank?
  end
end
