class SearchService
  extend LightService::Organizer

  ACTIONS = [
    CheckIfSearching,
    FindProduct,
    FindClosestCriteriaDestination
  ].freeze

  def self.call(params)
    with(
      product_reference: params[:product_reference],
      result: []
    ).reduce(ACTIONS)
  end
end
