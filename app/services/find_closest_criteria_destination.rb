class FindClosestCriteriaDestination
  extend ::LightService::Action

  expects :product
  promises :result

  executed do |context|
    @result = by_reference_or_category(context.product)

    context.result = order_result_by_matched_attributes(context.product).first

    if context.result.nil?
      context.fail! error_message(context.product.reference)
    end
  end

  class << self
    private

    def by_reference_or_category(product)
      CriteriaDefinition.matches_any_reference(product.reference)
        .or(CriteriaDefinition.contains_any_category(product.category))
        .less_than_price(product.price)
    end

    def order_result_by_matched_attributes(product)
      @result.order(
        Arel.sql(<<-SQL.squish
          CASE
            WHEN (
                '#{product.reference}' = ANY ("references")
                AND categories && '{#{product.category}}'
                AND maximum_price >= '#{product.price}'
              )
              THEN 3
            WHEN
                   ('#{product.reference}' = ANY ("references") AND categories && '{#{product.category}}' )
                OR ('#{product.reference}' = ANY ("references") AND maximum_price >= '#{product.price}')
                OR (categories && '{#{product.category}}' AND maximum_price >= '#{product.price}')
              THEN 2
            ELSE 1
          END
          DESC
        SQL
        )
      )
    end

    def error_message(reference)
      "No destination matches the parameters of the given product reference: #{reference}"
    end
  end
end
