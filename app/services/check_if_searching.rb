class CheckIfSearching
  extend ::LightService::Action

  expects :product_reference

  executed do |context|
    context.skip_remaining! if context.product_reference.blank?
  end
end
