class FindProduct
  extend ::LightService::Action

  expects :product_reference
  promises :product

  executed do |context|
    context.product = Product.find_by_reference(context.product_reference)

    if context.product.nil?
      context.fail_and_return!("No product with a reference of #{context.product_reference} was found")
    end
  end
end
