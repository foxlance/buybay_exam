class CriteriaDefinitionsController < ApplicationController
  before_action :set_criteria_definition, only: %i[ show edit update destroy ]

  # GET /criteria_definitions or /criteria_definitions.json
  def index
    @criteria_definitions = CriteriaDefinition.all
  end

  # GET /criteria_definitions/1 or /criteria_definitions/1.json
  def show
  end

  # GET /criteria_definitions/new
  def new
    @criteria_definition = helpers.decorate(CriteriaDefinition.new)
  end

  # GET /criteria_definitions/1/edit
  def edit
  end

  # POST /criteria_definitions or /criteria_definitions.json
  def create
    @criteria_definition = CriteriaDefinition.new(criteria_definition_params)

    respond_to do |format|
      if @criteria_definition.save
        format.html { redirect_to @criteria_definition, notice: "Criteria definition was successfully created." }
        format.json { render :show, status: :created, location: @criteria_definition }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @criteria_definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /criteria_definitions/1 or /criteria_definitions/1.json
  def update
    respond_to do |format|
      if @criteria_definition.update(criteria_definition_params)
        format.html { redirect_to @criteria_definition, notice: "Criteria definition was successfully updated." }
        format.json { render :show, status: :ok, location: @criteria_definition }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @criteria_definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /criteria_definitions/1 or /criteria_definitions/1.json
  def destroy
    @criteria_definition.destroy
    respond_to do |format|
      format.html { redirect_to criteria_definitions_url, notice: "Criteria definition was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_criteria_definition
      @criteria_definition = helpers.decorate(CriteriaDefinition.find(params[:id]))
    end

    # Only allow a list of trusted parameters through.
    def criteria_definition_params
      params.require(:criteria_definition).permit(:references, :categories, :maximum_price, :destination)
    end
end
