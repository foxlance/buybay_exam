class SearchController < ApplicationController
  # GET /search
  def index
    @search = SearchService.call(search_params)

    flash.now[:notice] = @search.message if @search.failure?
  end

  private

  def search_params
    params.permit(:product_reference)
  end
end
