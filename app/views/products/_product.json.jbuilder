json.extract! product, :id, :reference, :name, :category, :price, :created_at, :updated_at
json.url product_url(product, format: :json)
