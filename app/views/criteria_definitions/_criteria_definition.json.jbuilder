json.extract! criteria_definition, :id, :references, :categories, :maximum_price, :destination, :created_at, :updated_at
json.url criteria_definition_url(criteria_definition, format: :json)
