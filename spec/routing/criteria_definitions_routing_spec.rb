require "rails_helper"

RSpec.describe CriteriaDefinitionsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/criteria_definitions").to route_to("criteria_definitions#index")
    end

    it "routes to #new" do
      expect(get: "/criteria_definitions/new").to route_to("criteria_definitions#new")
    end

    it "routes to #show" do
      expect(get: "/criteria_definitions/1").to route_to("criteria_definitions#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/criteria_definitions/1/edit").to route_to("criteria_definitions#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/criteria_definitions").to route_to("criteria_definitions#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/criteria_definitions/1").to route_to("criteria_definitions#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/criteria_definitions/1").to route_to("criteria_definitions#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/criteria_definitions/1").to route_to("criteria_definitions#destroy", id: "1")
    end
  end
end
