require 'rails_helper'

RSpec.describe "criteria_definitions/index", type: :view do
  before(:each) do
    assign(:criteria_definitions, [
        CriteriaDefinition.create!(
          references: "REF1",
          categories: "console",
          maximum_price: 2.5,
          destination: "Rack 1"
      ),
        CriteriaDefinition.create!(
          references: "REF2",
          categories: "handheld",
          maximum_price: 2.5,
          destination: "Rack 2"
      )
    ])
  end

  it "renders a list of criteria_definitions" do
    render
    assert_select "tr>td", text: "REF1".to_s, count: 1
    assert_select "tr>td", text: "REF2".to_s, count: 1
    assert_select "tr>td", text: "console".to_s, count: 1
    assert_select "tr>td", text: "handheld".to_s, count: 1
    assert_select "tr>td", text: 2.5.to_s, count: 2
    assert_select "tr>td", text: "Rack 1".to_s, count: 1
    assert_select "tr>td", text: "Rack 2".to_s, count: 1
  end
end
