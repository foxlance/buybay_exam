require 'rails_helper'

RSpec.describe "criteria_definitions/edit", type: :view do
  before(:each) do
    @criteria_definition = assign(:criteria_definition, CriteriaDefinitionDecorator.new(
      CriteriaDefinition.create!(
      references: "GC1",
      categories: "console",
      maximum_price: 499.0,
      destination: "Special Rack"
    )))
  end

  it "renders the edit criteria_definition form" do
    render

    assert_select "form[action=?][method=?]", criteria_definition_path(@criteria_definition), "post" do

      assert_select "input[name=?]", "criteria_definition[references]"

      assert_select "input[name=?]", "criteria_definition[categories]"

      assert_select "input[name=?]", "criteria_definition[maximum_price]"

      assert_select "input[name=?]", "criteria_definition[destination]"
    end
  end
end
