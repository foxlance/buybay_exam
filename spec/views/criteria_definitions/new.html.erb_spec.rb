require 'rails_helper'

RSpec.describe "criteria_definitions/show", type: :view do
  before(:each) do
    @criteria_definition = assign(:criteria_definition, CriteriaDefinitionDecorator.new(
      CriteriaDefinition.create!(
        references: "",
        categories: "",
        maximum_price: 2.5,
        destination: "Destination"
    )))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/2.5/)
    expect(rendered).to match(/Destination/)
  end
end
