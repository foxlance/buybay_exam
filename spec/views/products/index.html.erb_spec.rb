require 'rails_helper'

RSpec.describe 'products/index', type: :view do
  before(:each) do
    assign(:products, [
             Product.create!(
               reference: 'Reference1',
               name: 'Product1',
               category: 'Category',
               price: 2.5
             ),
             Product.create!(
               reference: 'Reference2',
               name: 'Product2',
               category: 'Category',
               price: 2.5
             )
           ])
  end

  it 'renders a list of products' do
    render
    assert_select 'tr>td', text: 'Reference1'.to_s, count: 1
    assert_select 'tr>td', text: 'Reference2'.to_s, count: 1
    assert_select 'tr>td', text: 'Product1'.to_s, count: 1
    assert_select 'tr>td', text: 'Product2'.to_s, count: 1
    assert_select 'tr>td', text: 'Category'.to_s, count: 2
    assert_select 'tr>td', text: 2.5.to_s, count: 2
  end
end
