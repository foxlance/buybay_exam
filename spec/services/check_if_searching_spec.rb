require 'rails_helper'

RSpec.describe CheckIfSearching do
  subject(:resulting_context) { described_class.execute(context) }

  let(:context) do
    LightService::Context.new(product_reference: product_reference)
  end

  context "when product_reference is blank" do
    let(:product_reference) { nil }

    it "skips the remainder of the actions" do
      expect(resulting_context.success?).to eq true
      expect(resulting_context[:product]).to be_nil
      expect(resulting_context[:result]).to be_nil
    end
  end

  context "when product_reference is not blank" do
    let(:product_reference) { 'GC1' }

    it "skips the remainder of the actions" do
      expect(resulting_context.success?).to eq true
      expect(resulting_context.product_reference).to be_present
    end
  end
end
