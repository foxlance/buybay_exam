require 'rails_helper'

RSpec.describe SearchService do
  subject(:resulting_context) { described_class.call(product_reference: 'GC1') }

  let(:context) do
    LightService::Context.new(product_reference: 'GC1', result: [])
  end
  let(:expected_actions) do
    [
      CheckIfSearching,
      FindProduct,
      FindClosestCriteriaDestination
    ]
  end

  describe "ACTIONS" do
    it "should have the following actions" do
      expect(described_class::ACTIONS).to eq expected_actions
    end
  end

  describe "#call" do
    it "calls execute on all ACTIONS" do
      expected_actions.each do |action|
        allow(action).to receive(:execute).and_return(context)
      end

      resulting_context

      expect(expected_actions).to all have_received(:execute).with(context).once
    end
  end
end
