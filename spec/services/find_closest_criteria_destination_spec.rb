require 'rails_helper'

RSpec.describe FindClosestCriteriaDestination do
  subject(:resulting_context) { described_class.execute(context) }

  let(:context) do
    LightService::Context.new(product: product)
  end
  let!(:cheap_console_rack) do
    CriteriaDefinition.create(
      references: 'GC1',
      categories: 'console',
      maximum_price: 300,
      destination: 'Cheaper Console Rack'
    )
  end
  let!(:expensive_console_rack) do
    CriteriaDefinition.create(
      references: 'GC1',
      categories: 'console',
      maximum_price: 500,
      destination: 'Expensive Console Rack'
    )
  end
  let!(:handheld_rack) do
    CriteriaDefinition.create(
      references: 'GC1',
      categories: 'handheld',
      maximum_price: nil,
      destination: 'Handheld Rack'
    )
  end
  let!(:other_console_rack) do
    CriteriaDefinition.create(
      references: 'GC2',
      categories: 'console',
      maximum_price: 500,
      destination: 'Other Console Rack'
    )
  end

  context 'when product is valid' do
    let(:product) do
      Product.create(
        reference: 'GC1',
        name: 'Game Console',
        category: 'console',
        price: 399,
      )
    end

    it 'returns the criteria_destination meeting the most number of attribute matches' do
      expect(resulting_context.success?).to eq true
      expect(resulting_context.result).to eq expensive_console_rack
    end
  end

  context 'when product is valid' do
    let(:product) do
      Product.create(
        reference: 'TB3',
        name: 'iPad',
        category: 'tablet',
        price: 249,
      )
    end

    let(:expected_error_message) do
      "No destination matches the parameters of the given product reference: #{product.reference}"
    end

    it 'returns an error message' do
      expect(resulting_context.failure?).to eq true
      expect(resulting_context.result).to be_nil
      expect(resulting_context.message).to eq expected_error_message
    end
  end
end
