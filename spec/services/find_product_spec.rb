require 'rails_helper'

RSpec.describe FindProduct do
  subject(:resulting_context) { described_class.execute(context) }

  let(:context) do
    LightService::Context.new(product_reference: 'GC1')
  end

  context "when product_reference is valid" do
    let(:reference) { 'GC1' }
    let!(:product) do
      Product.create(
        reference: reference,
        name: 'product name',
        category: 'category',
        price: 499,
      )
    end

    it "assigns the product to context.product" do
      expect(resulting_context.success?).to eq true
      expect(resulting_context.product).to eq product
    end
  end

  context "when product_reference is invalid" do
    let(:reference) { 'GCX' }
    let!(:product) do
      Product.create(
        reference: reference,
        name: 'product name',
        category: 'category',
        price: 499,
      )
    end

    it "marks the context as a failure" do
      expect(resulting_context.failure?).to eq true
      expect(resulting_context.product).to be_nil
      expect(resulting_context.message).to eq "No product with a reference of GC1 was found"
    end
  end
end
