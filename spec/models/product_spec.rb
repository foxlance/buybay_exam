require 'rails_helper'

RSpec.describe Product, type: :model do
  subject { described_class.new }

  it 'is valid with valid attributes' do
    subject.reference = 'REF1'
    subject.name = 'Xbox'
    subject.category = 'console'
    subject.price = 499.0

    expect(subject).to be_valid
  end

  describe 'validation' do
    context 'when reference is already taken' do
      before do
        described_class.create(
          reference: 'REF1',
          name: 'Xbox',
          category: 'console',
          price: 499.0
        )
      end

      subject(:new_product) do
        described_class.create(
          reference: 'REF1',
          name: 'Xbox Series S',
          category: 'console',
          price: 399.0
        )
      end

      it 'fails the validation' do
        expect(new_product.valid?).to be_falsey
        expect(new_product.errors.full_messages).to include 'Reference has already been taken'
      end
    end

    context 'when parameters are invalid' do
      subject(:new_product) do
        described_class.create(
          reference: nil,
          name: nil,
          category: nil,
          price: nil
        )
      end
      let(:expected_errors) do
        [
          "Reference can't be blank",
          "Name can't be blank",
          "Category can't be blank",
          "Price can't be blank"
        ]
      end

      it 'fails the validation' do
        expect(new_product.valid?).to be_falsey
        expect(new_product.errors.full_messages).to eq expected_errors
      end
    end
  end
end
