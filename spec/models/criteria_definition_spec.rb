require 'rails_helper'

RSpec.describe CriteriaDefinition, type: :model do
  subject { described_class.new }

  it "is valid with valid attributes" do
    subject.references = "REF1, REF2"
    subject.categories = "console, handheld"
    subject.maximum_price = 499.0
    subject.destination = "Rack 1"

    expect(subject).to be_valid
  end

  describe "#references=" do
    let(:references) { ["REF1", "REF2"] }

    it "transform the incoming value into an array by comma separation" do
      subject.references = references.join(', ')

      expect(subject.references).to eq references
    end
  end

  describe "#categories=" do
    let(:categories) { ["console", "handheld"] }

    it "transform the incoming value into an array by comma separation" do
      subject.categories = categories.join(', ')

      expect(subject.categories).to eq categories
    end
  end
end
