Rails.application.routes.draw do
  resources :criteria_definitions
  resources :products
  get '/search', to: 'search#index'

  root to: 'search#index'
end
