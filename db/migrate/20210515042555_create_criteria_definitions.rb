class CreateCriteriaDefinitions < ActiveRecord::Migration[6.1]
  def change
    create_table :criteria_definitions do |t|
      t.string :references, array: true, default: []
      t.string :categories, array: true, default: []
      t.float :maximum_price
      t.string :destination

      t.timestamps
    end
  end
end
