class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :reference
      t.string :name
      t.string :category
      t.float :price

      t.timestamps
    end
    add_index :products, :reference, unique: true
  end
end
